package com.fereidooni.azimi.azmunazimi.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationViewPager;
import com.fereidooni.azimi.azmunazimi.CustomTypefaceSpan;
import com.fereidooni.azimi.azmunazimi.R;
import com.fereidooni.azimi.azmunazimi.fragment.MainFragment;
import com.fereidooni.azimi.azmunazimi.fragment.MainFragmentAdapter;

import java.util.ArrayList;

public class HomeActivity extends AppCompatActivity {

    private MainFragment currentFragment;
    private MainFragmentAdapter adapter;
    private ArrayList<AHBottomNavigationItem> bottomNavigationItems = new ArrayList<>();

    // UI
    private AHBottomNavigationViewPager viewPager;
    private AHBottomNavigation bottomNavigation;
    private FloatingActionButton floatingActionButton;
    private Typeface typeface;
    private boolean doubleBackToExitPressedOnce = false;
    private DrawerLayout drawerLayout;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_drawer_home);

        typeface = Typeface.createFromAsset(getAssets(),"fonts/iranian_sans.ttf");
        initUI();
        setUpToolbar();
        setUpNavigationView();

        viewPager.setOffscreenPageLimit(4);
        adapter = new MainFragmentAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapter);

        currentFragment = adapter.getCurrentFragment();


    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        if (drawerLayout != null)
            drawerLayout.closeDrawers();
    }

    private void initUI() {

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        }

        bottomNavigation = findViewById(R.id.bottom_navigation);
        viewPager = findViewById(R.id.view_pager);
        //floatingActionButton = findViewById(R.id.floating_action_button);

        AHBottomNavigationItem item1 = new AHBottomNavigationItem(R.string.tab_1, R.drawable.ic_thematic_exam, R.color.color_tab_1);
        AHBottomNavigationItem item2 = new AHBottomNavigationItem(R.string.tab_2, R.drawable.ic_general_exam, R.color.color_tab_2);
        AHBottomNavigationItem item3 = new AHBottomNavigationItem(R.string.tab_3, R.drawable.ic_personal_exam, R.color.color_tab_3);
        AHBottomNavigationItem item4 = new AHBottomNavigationItem(R.string.tab_4, R.drawable.ic_chart, R.color.color_tab_4);

        bottomNavigationItems.add(item1);
        bottomNavigationItems.add(item2);
        bottomNavigationItems.add(item3);
        bottomNavigationItems.add(item4);

        bottomNavigation.addItems(bottomNavigationItems);

        //bottomNavigation.manageFloatingActionButtonBehavior(floatingActionButton);
        bottomNavigation.setTranslucentNavigationEnabled(true);

        //bottomNavigation.setColored(true);

        bottomNavigation.setColored(true);

        bottomNavigation.setTitleTypeface(typeface);

        //bottomNavigation.setAccentColor(Color.parseColor("#F63D2B"));
        //bottomNavigation.setInactiveColor(Color.parseColor("#747474"));

        bottomNavigation.setTitleState(AHBottomNavigation.TitleState.SHOW_WHEN_ACTIVE);

        bottomNavigation.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener() {
            @Override
            public boolean onTabSelected(int position, boolean wasSelected) {


                if (currentFragment == null) {
                    currentFragment = adapter.getCurrentFragment();
                }

                if (wasSelected)
                    return true;

                if (currentFragment != null) {
                    currentFragment.willBeHidden();
                }

                viewPager.setCurrentItem(position, false);

                if (currentFragment == null) {
                    return true;
                }

                currentFragment = adapter.getCurrentFragment();
                currentFragment.willBeDisplayed();

                if (position == 0)
                {
                    toolbar.setBackgroundColor(getResources().getColor(R.color.color_tab_1));
                }
                else if (position == 1)
                {
                    toolbar.setBackgroundColor(getResources().getColor(R.color.color_tab_2));
                }
                else if (position == 2)
                {
                    toolbar.setBackgroundColor(getResources().getColor(R.color.color_tab_3));
                }
                else
                {
                    toolbar.setBackgroundColor(getResources().getColor(R.color.color_tab_4));
                }

                /*if (position == 1) {
                    bottomNavigation.setNotification("", 1);

                    floatingActionButton.setVisibility(View.VISIBLE);
                    floatingActionButton.setAlpha(0f);
                    floatingActionButton.setScaleX(0f);
                    floatingActionButton.setScaleY(0f);
                    floatingActionButton.animate()
                            .alpha(1)
                            .scaleX(1)
                            .scaleY(1)
                            .setDuration(300)
                            .setInterpolator(new OvershootInterpolator())
                            .setListener(new Animator.AnimatorListener() {
                                @Override
                                public void onAnimationStart(Animator animation) {

                                }

                                @Override
                                public void onAnimationEnd(Animator animation) {
                                    floatingActionButton.animate()
                                            .setInterpolator(new LinearOutSlowInInterpolator())
                                            .start();
                                }

                                @Override
                                public void onAnimationCancel(Animator animation) {

                                }

                                @Override
                                public void onAnimationRepeat(Animator animation) {

                                }
                            })
                            .start();

                } else {
                    if (floatingActionButton.getVisibility() == View.VISIBLE) {
                        floatingActionButton.animate()
                                .alpha(0)
                                .scaleX(0)
                                .scaleY(0)
                                .setDuration(300)
                                .setInterpolator(new LinearOutSlowInInterpolator())
                                .setListener(new Animator.AnimatorListener() {
                                    @Override
                                    public void onAnimationStart(Animator animation) {

                                    }

                                    @Override
                                    public void onAnimationEnd(Animator animation) {
                                        floatingActionButton.setVisibility(View.GONE);
                                    }

                                    @Override
                                    public void onAnimationCancel(Animator animation) {
                                        floatingActionButton.setVisibility(View.GONE);
                                    }

                                    @Override
                                    public void onAnimationRepeat(Animator animation) {

                                    }
                                })
                                .start();
                    }
                }*/

                return true;
            }
        });
    }

    private void setUpToolbar() {
        toolbar = findViewById(R.id.toolbar_home);
        setSupportActionBar(toolbar);

        DrawerLayout drawerLayout = findViewById(R.id.drawer_layout_home);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar,
                0, 0);

        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/iranian_sans.ttf");
        for (int i = 0; i < toolbar.getChildCount(); i++) {
            if (toolbar.getChildAt(i) instanceof TextView) {
                ((TextView) toolbar.getChildAt(i)).setTypeface(typeface);
            }
        }
    }

    private void setUpNavigationView() {
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setItemIconTintList(null);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                drawerLayout = findViewById(R.id.drawer_layout_home);
                switch (item.getItemId()) {
                    case R.id.nav_home_page:
                        drawerLayout.closeDrawers();
                        break;
                    case R.id.nav_important_questions:
                        Toast.makeText(HomeActivity.this, "سوالات کلیدی کلیک شد", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.nav_previous_exams:
                        //Toast.makeText(HomeActivity.this, "آزمون های گذشته کلیک شد", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(HomeActivity.this, PreviousExamsActivity.class));
                        break;
                    case R.id.nav_log_out:
                        Toast.makeText(HomeActivity.this, "خروج از حساب کاربری کلیک شد", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.nav_contact_us:
                        Toast.makeText(HomeActivity.this, "تماس با ما کلیک شد", Toast.LENGTH_SHORT).show();
                        break;
                }
                return true;
            }
        });

        setUpFontToNavigationView(navigationView);
    }

    private void setUpFontToNavigationView(NavigationView navigationView) {
        Menu navigationMenu = navigationView.getMenu();
        for (int i = 0; i < navigationMenu.size(); i++) {
            MenuItem menuItem = navigationMenu.getItem(i);
            SubMenu subMenu = menuItem.getSubMenu();
            if (subMenu != null && subMenu.size() > 0) {
                for (int j = 0; j < subMenu.size(); j++) {
                    MenuItem submenuItem = subMenu.getItem(j);
                    applyFontToMenuItem(submenuItem);
                }
            }
            applyFontToMenuItem(menuItem);
        }
    }

    private void applyFontToMenuItem(MenuItem mi) {
        SpannableString mNewTitle = new SpannableString(mi.getTitle());

        mNewTitle.setSpan(new CustomTypefaceSpan(this, "", typeface), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout_home);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            startActivity(intent);
        } else {

            if (currentFragment == null)
                currentFragment = adapter.getCurrentFragment();

            if (currentFragment != adapter.getItem(0))
            {
                currentFragment.willBeHidden();
                viewPager.setCurrentItem(0, false);
                bottomNavigation.setCurrentItem(0);
                currentFragment = adapter.getCurrentFragment();
                currentFragment.willBeDisplayed();
            }
            else
            {
                this.doubleBackToExitPressedOnce = true;
                Toast.makeText(this, "برای خروج دوباره دکمه بازگشت را فشار دهید.", Toast.LENGTH_SHORT).show();

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        doubleBackToExitPressedOnce = false;
                    }
                }, 1000);
            }
        }
    }

}
