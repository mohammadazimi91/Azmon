package com.fereidooni.azimi.azmunazimi.adaptor;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.fereidooni.azimi.azmunazimi.R;
import com.fereidooni.azimi.azmunazimi.model.PreviousExam;
import com.moghisfere.cmviews.CfTextView;

import java.util.List;

public class ExamResultRecyclerViewAdaptor extends RecyclerView.Adapter<ExamResultRecyclerViewAdaptor.ResultViewHolder> {
    private Context context;
    private List<PreviousExam> examList;

    public ExamResultRecyclerViewAdaptor(Context context, List<PreviousExam> examList) {
        this.context = context;
        this.examList = examList;
    }

    @NonNull
    @Override
    public ResultViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_previous_exam_summary, parent, false);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO: 2018-09-05 show complete info
                Toast.makeText(context, "به زودی قابل نمایش خواهد بود", Toast.LENGTH_SHORT).show();
            }
        });
        return new ResultViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ResultViewHolder holder, int position) {
        holder.textExamType.setText(examList.get(position).getExamType());
        holder.textExamNumber.setText(examList.get(position).getExamNumber());
        holder.textExamDate.setText(examList.get(position).getExamDate());
        holder.textExamResult.setText(examList.get(position).getExamResult());
    }

    @Override
    public int getItemCount() {
        return examList.size();
    }

    protected class ResultViewHolder extends RecyclerView.ViewHolder {
        protected CfTextView textExamType;
        protected CfTextView textExamNumber;
        protected CfTextView textExamDate;
        protected CfTextView textExamResult;

        public ResultViewHolder(View itemView) {
            super(itemView);

            textExamType = itemView.findViewById(R.id.text_summary_exam_type);
            textExamNumber = itemView.findViewById(R.id.text_summary_exam_number);
            textExamDate = itemView.findViewById(R.id.text_summary_exam_date);
            textExamResult = itemView.findViewById(R.id.text_summary_exam_result);
        }
    }
}
