package com.fereidooni.azimi.azmunazimi.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fereidooni.azimi.azmunazimi.R;
import com.fereidooni.azimi.azmunazimi.activity.HomeActivity;
import com.fereidooni.azimi.azmunazimi.activity.MainActivity;
import com.moghisfere.cmviews.CfButton;

public class CollectionFragment extends Fragment {
    CfButton buttonOldCollection;
    CfButton buttonNewCollection;
    public CollectionFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_collection, container, false);
        initCollectionView(view);
        return view;
    }

    private void initCollectionView(View view) {
        buttonNewCollection = view.findViewById(R.id.button_starter_new);
        buttonOldCollection = view.findViewById(R.id.button_starter_old);

        buttonNewCollection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO: 2018-09-06 change to fragment
                Intent intent = new Intent(getContext(), HomeActivity.class);
                //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });
        buttonOldCollection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO: 2018-09-06 change to fragment
                Intent intent = new Intent(getContext(), HomeActivity.class);
                //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });
    }

}
