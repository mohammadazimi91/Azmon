package com.fereidooni.azimi.azmunazimi.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.fereidooni.azimi.azmunazimi.R;
import com.fereidooni.azimi.azmunazimi.activity.MainActivity;

public class StarterFragment extends Fragment {
    private Button start;

    public StarterFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View view =  inflater.inflate(R.layout.fragment_starter, container, false);
            initStarterView(view);
            return view;
    }

    private void initStarterView(View view) {
        start = view.findViewById(R.id.button_welcome);
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)requireActivity()).replaceAddToStack(new CollectionFragment());
            }
        });
    }
}
